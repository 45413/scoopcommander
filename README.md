# ScoopCommander

## Requirements

- Powershell v5+ or [Powershell Core](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-windows?view=powershell-7.1) (v6+)
- [scoop](https://scoop.sh)

## Installation from PowershellGallery - ⚠ COMING SOON ⚠

```powershell
Install-Module -Name ScoopCommander -Scoop CurrentUser
```

## Building module from source

### Required Modules

- Pester
- platyPS
- psake
- PSScriptAnalyzer (optional)

To build and install module locally from source.

```powershell
Invoke-psake build.psake.ps1 -taskList Install
```