param (
    [bool]$DebugModule = $false
)

# Get public and private function definition files
$Public = @( Get-ChildItem -Path $PSScriptRoot\Public\*.ps1 -ErrorAction SilentlyContinue )
$Private = @( Get-ChildItem -Path $PSScriptRoot\Private\*.ps1 -ErrorAction SilentlyContinue )
$FilesToLoad = @([object[]]$Public + [object[]]$Private) | Where-Object { $_ }
$ModuleRoot = $PSScriptRoot

# Dot source the files
# Thanks to Bartek, Constatine
# https://becomelotr.wordpress.com/2017/02/13/expensive-dot-sourcing/
Foreach ($File in $FilesToLoad) {
    Try {
        if ($DebugModule) {
            . $File.FullName
        }
        else {
            . (
                [scriptblock]::Create(
                    [io.file]::ReadAllText($File.FullName, [Text.Encoding]::UTF8)
                )
            )
        }
    }
    Catch {
        Write-Error -Message "Failed to import function $($File.fullname)"
        Write-Error $_
    }
}

# Check if scoop is installed
try {
    $Script:ScoopRoot = @($env:SCOOP, (Get-ScoopParameter -Name "rootPath"), "$env:USERPROFILE\scoop") |
        Where-Object { -not [String]::IsNullOrEmpty($_) } | Select-Object -First 1
}
catch { Write-Warning 'Scoop is not insalled! Download scoop from https://scoop.sh/' }

# Only export public functions that follow the Verb-Noun naming convention
Export-ModuleMember -Function ( $Public.Basename | Where-Object { $_ -like "*-*" } )
