
function Set-ScoopVersion {
    [cmdletBinding(ConfirmImpact = "Medium", SupportsShouldProcess = $true)]
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSReviewUnusedParameter', '', Justification = 'May need access to previously bound command parameters')]
    param (
        # App Name
        [Parameter(Mandatory, Position = 0)]
        [ArgumentCompleter( {
                (Get-ChildItem -Directory -Path "${env:HOME}\scoop\apps").Name
            })]
        [ValidateNotNullOrEmpty()]
        [Alias("Name")]
        [string]
        $App,
        # Version
        [Parameter(Mandatory, Position = 1)]
        [ArgumentCompleter( {
                param($Command, $Parameter, $WordToComplete, $CommandAst, $FakeBoundParams)
                Get-ScoopVersion @FakeBoundParams -ListAllVersions
            })]
        [ValidateNotNullOrEmpty()]
        [String]
        $Version
    )

    $appDir = "${env:HOME}\scoop\apps\${app}"

    if (Test-Path -Path $appDir ) {
        Write-Verbose "Found app `"${app}`" at ${appDir}"

        # check if version is valid
        if ((Get-ScoopVersion -App "terraform" -ListAllVersions) -contains $Version) {
            $current = Get-Item -Path "${appDir}\current"

            # Remove ReadOnly attribute if present
            if ($current.Attributes.HasFlag([System.IO.FileAttributes]::ReadOnly)) {
                $current.Attributes -= [System.IO.FileAttributes]::ReadOnly
            }

            # Delete the existing link and recreate
            Remove-Item -Path  "${appDir}\current"
            $current = New-Item -ItemType Junction -Path "${appDir}\current" -Value "${appDir}\${Version}" -Force
            # Set ReadOnly attribute
            $current.Attributes += [System.IO.FileAttributes]::ReadOnly
        }
        else {
            Write-Error "Invalid version for scoop app: ${app}. Use `"Get-ScoopVersion -App ${app} -ListAllVersions`" to list available versions"
            return
        }
    }
    else {
        Write-Error "Scoop app: ${app}, no found. Please check name and try again."
        return
    }

}