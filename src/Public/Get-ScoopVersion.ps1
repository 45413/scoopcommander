function Get-ScoopVersion {
    [cmdletBinding()]
    param (
        # App Name
        [Parameter(Mandatory, Position = 0)]
        [ArgumentCompleter( {
                (Get-ChildItem -Directory -Path "${env:HOME}\scoop\apps").Name
            })]
        [ValidateNotNullOrEmpty()]
        [Alias("Name")]
        [string]
        $App,
        # List all versions
        [Parameter(Mandatory = $false)]
        [Switch]
        $ListAllVersions
    )

    $appDir = "${env:HOME}\scoop\apps\${app}"
    if (Test-Path -Path $appDir ) {
        Write-Debug "Found app `"${app}`" at ${appDir}"

        if ($ListAllVersions) {
            $version = @(Get-ChildItem -Path $appDir -Exclude "current" | Select-Object -ExpandProperty Name)
        }
        else {
            $version = (Get-Item -Path "${appDir}\current").Target.Split('\')[-1]
        }
        return $version
    }
    else {
        Write-Error "Scoop app: ${app}, no found. Please check name and try again."
        return
    }

}