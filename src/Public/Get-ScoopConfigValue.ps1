function Get-ScoopParameter {
    [CmdletBinding()]
    param (
        # Config Paramater Name
        [Parameter(Mandatory,
            Position = 0)]
        [ArgumentCompleter({
            (Read-Config "${env:USERPROFILE}\.config\scoop\config.json").psobject.Properties.Name
        })]
        [ValidateNotNullOrEmpty()]
        [Alias("ParameterName")]
        [String]
        $Name,
        # Default Value if Parameter not set.
        [Parameter(Mandatory = $false,
            position = 1)]
        [String]
        $Default
    )

    $configFile = "${env:USERPROFILE}\.config\scoop\config.json"
    $config = Read-Config -Path $configFile

    $value = $config.$Name

    if ($null -eq $value -and $null -ne $Default) {
        $value = $Default
    }

    return $value
}