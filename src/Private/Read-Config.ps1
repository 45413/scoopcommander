function Read-Config {
    [CmdletBinding()]
    param (
        # Specifies a path to one or more locations.
        [Parameter(Mandatory = $true,
            Position = 0,
            HelpMessage = "Path to one or more locations.")]
        [Alias("PSPath")]
        [ValidateNotNullOrEmpty()]
        [string]
        $Path
    )

    $config = try {
        Get-Content $Path -Raw -ErrorAction Stop | ConvertFrom-Json -ErrorAction Sto
    }
    catch {
        Write-Verbose "Unable to read config: $Path"
        $null
    }

    return $config
}